resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name != null ? var.resource_group_name : "rg-acr-${var.name}"
  location = var.location
  tags     = var.tags
}

resource "azurerm_container_registry" "acr" {
  name                = var.name
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = "Basic"
  admin_enabled       = false
}

resource "azurerm_role_assignment" "aks_acr_role_assignment" {
  for_each = var.aks_object_ids

  scope                = azurerm_container_registry.acr.id
  role_definition_name = "AcrPull"
  principal_id         = each.value
}
