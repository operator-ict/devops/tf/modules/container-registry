variable "name" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "location" {
  type = string
}
variable "resource_group_name" {
  type    = string
  default = null
}

variable "aks_object_ids" {
  type    = map(string)
  default = {}
}
